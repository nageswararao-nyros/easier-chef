import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Grid from '@material-ui/core/Grid';

import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {
  Link
} from "react-router-dom";
import { useHistory } from "react-router-dom";
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Parser from 'html-react-parser';


var ingredients = require('../src/ingredients.json');

export default class Home extends Component {

	constructor(props){
		super(props);

		this.state = {
			dishes_data: [],
			have_filter: false,
			tags: ingredients,
		}

		this.onRecipieFilter = this.onRecipieFilter.bind(this);
	}

	componentDidMount = () => {
		const dishes_data = localStorage.getItem('dishes')
		if(dishes_data == null){
			this.setState({
				dishes_data: []
			})
		}else{
			const dishes_arr = JSON.parse(dishes_data)
			this.setState({
				dishes_data: dishes_arr,
				backup_data: dishes_arr
			})
		}
  }


  onRecipieFilter(item, data){
  	var result = [
					  {
					    "name": "sasa",
					    "recipie": "sugar",
					    "url": "https://material-ui.com/components/text-fields/#text-field",
					    "number": "22",
					    "steps": ""
					  },
					  {
					    "name": "sasasa",
					    "recipie": "black pepper",
					    "url": "http://localhost:3000/add",
					    "number": "32",
					    "steps": ""
					  },
					  {
					    "name": "sasasa",
					    "recipie": "black pepper",
					    "url": "http://localhost:3000/add",
					    "number": "32",
					    "steps": ""
					  },
					  {
					    "name": "Biryani",
					    "recipie": "unsalted butter",
					    "url": "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?quality=90&resize=700%2C636",
					    "number": "21",
					    "steps": "<h3 style=\"font-family: Roboto; box-sizing: border-box; margin: 27px 0px 17px; padding: 0px; color: rgb(17, 17, 17); font-weight: 500; font-size: 22px; line-height: 30px; text-align: start; background-color: rgb(255, 255, 255);\">For marination</h3><h1><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 1</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">In a bowl, whisk yoghurt, ginger garlic paste, coriander powder, salt, lemon juice, turmeric powder and red chilli powder. Also, add mint leaves to the bowl. Mix it together and add chicken pieces to the mixture. Allow it to marinate for 30 minutes.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 2</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">Boil rice in a steamer. Once the rice is cooked, remove it from the water and keep it aside.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 3</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">In a wok, fry sliced onions till they turn brown and crispy. Keep it aside in a plate.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 4</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">In the same wok, add oil, cumin seeds, cloves, cardamom, bay leaf and cashew nuts. Saute fry it for 1 minute. Now, add chopped tomatoes and potato.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 5</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">Add garam masala, red chilli powder and salt. Place one layer of rice in the pan, followed by chicken pieces. Next, add the fried onions over it. Again add a layer of rice, chicken pieces, onions and repeat a layer of rice. 7. Now, close the wok with a lid and allow it to cook for about 20 – 25 minutes.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 6</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">Once the chicken is cooked, add saffron milk to the rice and cover it again for 2 minutes.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 7</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">Garnish it with coriander leaves, fried onion and dry fruits.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 8</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">Serve hot with boondi or pineapple raita.</p></h1><div><br></div>"
					  },
					  {
					    "name": "sasasa",
					    "recipie": "black pepper",
					    "url": "https://cdn.cdnparenting.com/articles/2018/11/22172711/New-Project-696x476.jpg",
					    "number": "32",
					    "steps": "<h3 style=\"font-family: Roboto; box-sizing: border-box; margin: 27px 0px 17px; padding: 0px; color: rgb(17, 17, 17); font-weight: 500; font-size: 22px; line-height: 30px; text-align: start; background-color: rgb(255, 255, 255);\">For marination</h3><h1><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 1</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">In a bowl, whisk yoghurt, ginger garlic paste, coriander powder, salt, lemon juice, turmeric powder and red chilli powder. Also, add mint leaves to the bowl. Mix it together and add chicken pieces to the mixture. Allow it to marinate for 30 minutes.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 2</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">Boil rice in a steamer. Once the rice is cooked, remove it from the water and keep it aside.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 3</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">In a wok, fry sliced onions till they turn brown and crispy. Keep it aside in a plate.</p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px;\">Step 4</strong></p><p style=\"font-family: Roboto; box-sizing: border-box; margin: 0px 0px 26px; padding: 0px; font-size: 15px; line-height: 1.74; font-weight: 300; overflow-wrap: break-word; color: rgb(34, 34, 34); text-align: start; background-color: rgb(255, 255, 255);\">In the same wok, add oil, cumin seeds, cloves, cardamom, bay leaf and cashew nuts. Saute fry it for 1 minute. Now, add chopped tomatoes and potato.</p><div><br></div></h1>"
					  },
					  {
					    "name": "sasasasa",
					    "recipie": "salt",
					    "url": "https://images.unsplash.com/photo-1494548162494-384bba4ab999?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZGF3bnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
					    "number": "21",
					    "steps": "<h1>https://images.unsplash.com/photo-1494548162494-384bba4ab999?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZGF3bnxlbnwwfHwwfA%3D%3D&amp;ixlib=rb-1.2.1&amp;w=1000&amp;q=80</h1>"
					  },
					  {
					    "name": "sasasasa",
					    "recipie": "salt",
					    "url": "https://images.unsplash.com/photo-1494548162494-384bba4ab999?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZGF3bnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
					    "number": "21",
					    "steps": "<h1>https://images.unsplash.com/photo-1494548162494-384bba4ab999?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZGF3bnxlbnwwfHwwfA%3D%3D&amp;ixlib=rb-1.2.1&amp;w=1000&amp;q=80</h1>"
					  },
					  {
					    "name": "sasasasa",
					    "recipie": "salt",
					    "url": "https://images.unsplash.com/photo-1494548162494-384bba4ab999?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZGF3bnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
					    "number": "21",
					    "steps": "<h1>https://images.unsplash.com/photo-1494548162494-384bba4ab999?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZGF3bnxlbnwwfHwwfA%3D%3D&amp;ixlib=rb-1.2.1&amp;w=1000&amp;q=80</h1>"
					  },
					  {
					    "name": "dadasdas",
					    "recipie": "black pepper",
					    "url": "https://images.unsplash.com/photo-1494548162494-384bba4ab999?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZGF3bnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
					    "number": "32",
					    "steps": "<h1><p style=\"box-sizing: inherit; margin: 0.5em 0px 1.3em; color: rgb(255, 255, 255); font-family: &quot;Proxima Nova&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen-Sans, Ubuntu, Cantarell, &quot;Helvetica Neue&quot;, sans-serif; font-size: 20px; font-weight: 400; text-align: start; background-color: rgb(40, 44, 53);\">Easy peasy. Worse, IMO. But, easy peasy.</p><p style=\"box-sizing: inherit; margin: 0.5em 0px 1.3em; color: rgb(255, 255, 255); font-family: &quot;Proxima Nova&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen-Sans, Ubuntu, Cantarell, &quot;Helvetica Neue&quot;, sans-serif; font-size: 20px; font-weight: 400; text-align: start; background-color: rgb(40, 44, 53);\">Now, what if the&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">Register</code>&nbsp;component wasn’t being rendered by React Router? (Meaning, we’re not passing&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">Register</code>&nbsp;as a&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">component</code>&nbsp;prop to a&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">Route</code>. Instead, we’re just rendering it ourselves like&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">&lt;Register /&gt;</code>). If it’s not rendered by React Router, then we won’t have access to&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">history</code>,&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">match</code>, or&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">location</code>, which means we also won’t have access to&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">history.push</code>. To fix this and get access to those props, you can use the&nbsp;<code class=\"language-text\" style=\"box-sizing: inherit; background: var(--gray); padding: 3px; color: rgb(192, 197, 206); font-family: &quot;Fira Mono&quot;, SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 18px; line-height: 1.5; direction: ltr; word-spacing: normal; word-break: normal; tab-size: 2; hyphens: none; border-radius: 0.3em; margin: 30px 0px !important;\">withRouter</code>&nbsp;higher-order component.</p></h1>"
					  }
					]
			    data.map(data_item=>{
					result = result.filter(word => word.recipie == data_item.searchValue)
				})
			  	return result.length;
			  }

	render(){
	  return (
	    <div>
	      <div className="grid_block">
		      <Grid container style={{padding: 25, alignItems:'center'}} >
		      	<Grid item xs={6} sm={6}>
		      	  <Autocomplete
				        multiple
				        limitTags={2}
				        id="multiple-limit-tags"
				        options={this.state.tags}
                onChange={this.onRecipieFilter}
				        getOptionLabel={(option) => option.searchValue}
				        defaultValue={[]}
				        renderInput={(params) => (
				          <TextField {...params} variant="outlined" label="recipies" placeholder="Recipies" />
				        )}
				      />
		      	</Grid>
		      	<Grid style={{textAlign:'right',}} item xs={6} sm={6}>
		      	  <Link  to="/add" style={{padding: 10, backgroundColor:'blue', textDecoration:'none',  color: '#fff'}}>Add Dish</Link>
		      	</Grid>
		      </Grid>
		      <Grid className="list" container style={{paddingTop: 20,}} >
		      	{
		      		this.state.dishes_data.length > 0 ?
		      			this.state.dishes_data.map(item=>{
		      				return(
		      					<Grid item xs={6} sm={3}>
							        <Card style={{margin: 10}}>
									      <CardHeader
									        title={
								        		item.name
								        	}
									        subheader="September 14, 2016"
									      />
									      <CardMedia
									        style={{height: 200, width:'100%'}}
									        image={
								        		item.url
								        	}
								        	onerror="https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?quality=90&resize=700%2C636"
									        title="Paella dish"
									      />
									      <p style={{textAlign:'left', paddingLeft: 10}}>Major Ingredient: {item.recipie}</p>
									      <CardContent>
								        	<p style={{height: 100, overflowY:'scroll'}}>{
								        		Parser(item.steps)
								        	}</p>
									      </CardContent>
									    </Card>
							      </Grid>
		      				)
		      			})
		      		:
		      		<h1>No dishes available</h1>
		      	}
			    </Grid>
	    	</div>
	    </div>
	  );
	}
}