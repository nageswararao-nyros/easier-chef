import Home from './home';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import React from 'react';
import Grid from '@material-ui/core/Grid';

configure({adapter: new Adapter()});

const not_available = [{ "id" : "71b3ae9a-c29a-9f56-f1b0cb8247932bdc",
										    "ingredientId" : "657ff16c-114f-49e3-a673-e48e5a163ab8",
										    "searchValue" : "lemon juice",
										    "term" : "lemon juice",
										    "useCount" : 134487
										  }]
const available  = [{ "id" : "71b70e8e-c29a-9f56-f1933ea67ee267cd",
									    "ingredientId" : "30b7a1fc-9c68-43e8-8bf8-1a0d83aa1788",
									    "searchValue" : "black pepper",
									    "term" : "black pepper",
									    "useCount" : 355591
									  }]

test('Dish not available', () => {
  const wrapper = shallow(<Home />)
  const insta = wrapper.instance();
  expect(insta.onRecipieFilter(0, not_available)).toBe(0);
});

test('Dish available', () => {
  const wrapper = shallow(<Home />)
  const insta = wrapper.instance();
  expect(insta.onRecipieFilter(0, available)).toBeGreaterThan(1);
});

test('Have Dishes', () => {
  const wrapper = shallow(<Home />)
  expect(wrapper.find('.grid_block').length).toBe(1)
});
