import React, {Component} from 'react';
// import logo from './logo.svg';
// import './App.css';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Editor from 'material-ui-editor';
import {
  Redirect
} from "react-router-dom";
var ingredients = require('../ingredients.json');

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  card:{
  	width: '95%',
  	margin: 10
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    textAlign:'left'
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
    
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  // avatar: {
  //   backgroundColor: red[500],
  // },
}));

export default class AddDish extends Component {
  // const classes = useStyles();
  constructor(props){
    super(props);
    this.state = {
      url_error: false,
      name_error: false,
      number_error: false,
      recipie_error: false,
      pic_error: false,
      name: '',
      number: '',
      url: '',
      steps: '',
      recipie: '',
      unit: 5
    }

    this.onTextChange = this.onTextChange.bind(this);
    this.onRecipieChange = this.onRecipieChange.bind(this)
  }

  componentDidMount = () =>{
    console.log(ingredients)
  }

  AddDishItem(){
    var regex = /(http(s?):)|([/|.|\w|\s])*\.(?:jpg|gif|png)/g
    var validate = regex.test(this.state.url)
    if(this.state.name == "" || this.state.number == '' || validate == false || this.state.url == '' || this.state.recipie == ""){
      if(!validate){
        alert("Enter proper URL")
        this.setState({
          url_error: true
        })
      }
      if(this.state.name == ""){
        this.setState({
          name_error: true
        })
      }
      if(this.state.number == ""){
        this.setState({
          number_error: true
        })
      }
      if(this.state.recipie == ""){
        alert("Atlease one recipie required")
      }
    }else{
      var data = {
        name: this.state.name,
        recipie: this.state.recipie,
        url: this.state.url,
        number: this.state.number,
        steps: this.state.steps
      }
      var dish;
      if(localStorage.getItem('dishes') == null){
        dish = [];
      }else{
        var old_data = localStorage.getItem('dishes')
        var old_data_arr = JSON.parse(old_data)
        // console.log(old_data_arr)
        dish = old_data_arr;
      }
      dish.push(data);
      localStorage.setItem('dishes', JSON.stringify(dish))
      // alert("Done")
      window.location.href = "/"
    }
  }

  onTextChange(item, type){
    this.setState({
      [type] : item
    })

    if(type == "number"){
      // alert(item)
      this.setState({
        unit: 100
      })
    }
  }

  onRecipieChange(item, value){
    this.setState({
      recipie: value.searchValue
    })
  }

  render(){
    return (
      <div>
        <div style={{width: '80%', paddingTop: 50, margin: '0 auto'}}>
          <TextField defaultValue={this.state.name} error={this.state.name_error} onChange={(item)=>this.onTextChange(item.target.value, 'name')} style={{width: '100%'}} id="outlined-basic" label="Name" variant="outlined" />
          <div className="add_ingredient">
            <Grid container spacing={2} style={{paddingTop: 20}}>
              <Grid item md={4} lg={4} xs={12} sm={12}>
                <Autocomplete
                  id="highlights-demo"
                  error={this.state.recipie_error}
                  // style={{ width: 300 }}
                  onChange={this.onRecipieChange}
                  options={ingredients}
                  getOptionLabel={(option) => option.searchValue}
                  defaultValue={[]}
                  renderInput={(params) => (
                    <TextField {...params} variant="outlined" label="recipies" placeholder="Recipies" />
                  )}
                />
              </Grid>
              <Grid item md={4} lg={4} xs={12} sm={12}>
                <TextField
                  error={this.state.number_error}
                  id="filled-number"
                  label="Count"
                  type="number"
                  defaultValue={this.state.number}
                  variant="outlined"
                  style={{width:'100%'}}
                  onChange={(item)=>this.onTextChange(item.target.value, 'number')}
                />
              </Grid>
              <Grid item md={4} lg={4} xs={12} sm={12}>
                <TextField
                  id="filled-read-only-input"
                  label="Gms"
                  // error={this.state.error}
                  defaultValue="Unit"
                  InputProps={{
                    readOnly: true,
                  }}
                  defaultValue={this.state.unit + " gms"}
                  // onChange={(item)=>this.onTextChange(item.target.value, 'unit')}
                  style={{width:'100%'}}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <div style={{marginTop: 20, width:'100%'}}>
              <TextField
                style={{width:'100%'}}
                error={this.state.url_error}
                id="filled-read-only-input"
                label="Picture URL"
                defaultValue={this.state.url}
                onChange={(item)=>this.onTextChange(item.target.value, 'url')}
                variant="outlined"
              />
            </div>
            <div style={{marginTop: 20, width:'100%'}}>
              <Editor content={"<h1>Steps to Prepare</h1>"}
                onChange={(item)=>this.onTextChange(item, 'steps')}
              />
            </div>
            <div style={{marginTop: 20, marginBottom: 20, width:'100%'}}>
              <Button onClick={ ()=>this.AddDishItem() } style={{width:'100%', padding: 10, marginTop:10}} variant="contained" color="primary"> Add Dish </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// export default AddDish;
