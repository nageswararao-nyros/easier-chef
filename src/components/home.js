import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Grid from '@material-ui/core/Grid';

import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {
  Link
} from "react-router-dom";
import { useHistory } from "react-router-dom";
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Parser from 'html-react-parser';


var ingredients = require('../ingredients.json');

export default class Home extends React.Component {

	constructor(props){
		super(props);

		this.state = {
			dishes_data: [],
			have_filter: false,
			tags: ingredients,
		}

		this.onRecipieFilter = this.onRecipieFilter.bind(this);
	}

	componentDidMount = () => {
		const dishes_data = localStorage.getItem('dishes')
		if(dishes_data == null){
			this.setState({
				dishes_data: []
			})
		}else{
			const dishes_arr = JSON.parse(dishes_data)
			this.setState({
				dishes_data: dishes_arr,
				backup_data: dishes_arr
			})
		}
  }


  onRecipieFilter(item, data){
  	var result = this.state.backup_data;

  	if(data.length > 0){
  		data.map(data_item=>{
  			result = this.state.backup_data.filter(word => word.recipie == data_item.searchValue)
  		})
  		this.setState({
  			dishes_data: result
  		})
  	}else{
  		this.setState({
  			dishes_data: this.state.backup_data
  		})
  	}
  }

	render(){
	  return (
	    <div>
	      <div>
		      <Grid container style={{padding: 25, alignItems:'center'}} >
		      	<Grid item xs={6} sm={6}>
		      	  <Autocomplete
				        multiple
				        limitTags={2}
				        id="multiple-limit-tags"
				        options={this.state.tags}
                onChange={this.onRecipieFilter}
				        getOptionLabel={(option) => option.searchValue}
				        defaultValue={[]}
				        renderInput={(params) => (
				          <TextField {...params} variant="outlined" label="recipies" placeholder="Recipies" />
				        )}
				      />
		      	</Grid>
		      	<Grid style={{textAlign:'right',}} item xs={6} sm={6}>
		      	  <Link  to="/add" style={{padding: 10, backgroundColor:'blue', textDecoration:'none',  color: '#fff'}}>Add Dish</Link>
		      	</Grid>
		      </Grid>
		      <Grid container style={{paddingTop: 20,}} >
		      	{
		      		this.state.dishes_data.length > 0 ?
		      			this.state.dishes_data.map(item=>{
		      				return(
		      					<Grid item xs={6} sm={3}>
							        <Card style={{margin: 10}}>
									      <CardHeader
									        title={
								        		item.name
								        	}
									        subheader="September 14, 2016"
									      />
									      <CardMedia
									        style={{height: 200, width:'100%'}}
									        image={
								        		item.url
								        	}
								        	onerror="https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?quality=90&resize=700%2C636"
									        title="Paella dish"
									      />
									      <p style={{textAlign:'left', paddingLeft: 10}}>Major Ingredient: {item.recipie}</p>
									      <CardContent>
								        	<p style={{height: 100, overflowY:'scroll'}}>{
								        		Parser(item.steps)
								        	}</p>
									      </CardContent>
									    </Card>
							      </Grid>
		      				)
		      			})
		      		:
		      		<h1>No dishes available</h1>
		      	}
			    </Grid>
	    	</div>
	    </div>
	  );
	}
}