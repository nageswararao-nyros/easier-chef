import React from 'react';
import logo from './logo.svg';
import './App.css';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Route, BrowserRouter, Router } from "react-router-dom";

// components
import Home from './components/home';
import AddDish from './components/add_dish';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  card:{
  	width: '95%',
  	margin: 10
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    textAlign:'left'
  },
  
}));

function App() {
	const classes = useStyles();

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
        	<IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            EasierChef
          </Typography>
          <Button color="inherit">Logout</Button>
        </Toolbar>
      </AppBar>

      <BrowserRouter>
		    <div className="App" >
		      <Route exact path="/" component={Home} />
		      <Route exact path="/add" component={AddDish}/>
		    </div>
	    </BrowserRouter>
    </div>
  );
}

export default App;
